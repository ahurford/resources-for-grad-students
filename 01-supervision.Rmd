# Supervision {#supervision}

At Memorial University, the expectations of supervisor and student are outlined [here](./responsibilities.pdf).

## My supervisory style
My primary priority is to help you graduate your degree. My secondary priority is for the research contained in your thesis to be of as high quality as possible, to maximize your chances of success on the job market. In addition, the topic and research contained in the thesis needs to be within the scope of my research group, as discussed during our initial meetings. I also expect that you respect ethical standards of good science.

The principle way that I supervise is by providing feedback on items you have completed. This may be a research proposal, or a written summary of your work during the past month, or a draft of a paper. Initially, I would like to have contact with each new student once every 1-2 weeks (depending on your workload), but as our working relationship establishes, we can meet when there are completed items to discuss.

With regards to our working relationship you might consider the following:

- It can be difficult to tell your supervisor, or other graduate students that you don't know how to do something. Will they think you are not smart and don't belong in graduate school? Isn't mathematical biology supposed to be what _you're_ good at; how can you ask a student from another lab about math biology? You can make a lot of progress on your thesis by asking other graduate students for help with your coding, talking through the interpretation of a result, the logic behind a hypothesis, or getting feedback on your writing. The most sucessful graduate students are comfortable getting help from other graduate students.

- Developing as an independent researcher is a goal of your studies. There is some element of being stuck that is a normal part of research, and overcoming the problem yourself may be the best approach for your professional development. However, this isn't always the case, and sometimes you should seek help rather than trying to keep working alone.

- If your work is not moving forward, you might want to schedule a meeting with me. If things are not working out as I said they would, then we need to figure out what is wrong. If you've been stuck on that coding problem for too long, let me know to look at your code.

- If you do not agree with something substantial that we discussed in a previous meeting, we need to have a another meeting to discuss the new best approach.

There are some aspects of graduate student supervision that you may not be aware of: I write grants to secure funding that goes toward your graduate student salary; I write reports on how these funds were used; I send emails to organize supervisory committees; and I serve on the supervisory and examination committees of other graduate students. I spent a substantial amount of time on activities that are indirectly related to supporting your studies. I want you to succeed, and if you do good work and succeed mostly due to your own hard work, I will notice and write about this in my recommendation letters for you.

## Mentoring plan
When you enter, and after each year subsequently, we will meet to discuss your career plans. The emphasis is both of us to decide on research that will move you towards your long-term career objectives, but also that is acheivable given the recommended degree lengths for MUN (Masters - 2 years; PhD - 4 years).
