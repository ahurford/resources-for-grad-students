# Your thesis

## Hypotheses and models

1. Design a chapter that is do-able. In my experience, a plan that is 'just a bit more' than the current state-of-the art, will turn into something bigger, and might even turn out to be amazing. A good thesis is a done thesis. Make the hard decisions early: it is less glamorous to design a thesis that is feasible, but it will get you graduated and get you a job.

2. Getting a chapter finished is a great accomplishment.

3. If you plan for a career in academia, it can be helpful to work collaboratively. Know the guidelines for authorship. If you would like to involve a co-author, you need to discuss this with me.

4. Consider Zotero for managing your references. Use the browser plug-in and the add-on for MS Word. Bibtex is another good choice.

5. Read the literature to formulate your hypotheses. Every thesis is framed within a broader context, for example,
- population dynamics in seasonal environments;
- population dynamics in stochastic environments;
- population dynamics given an earlier age of first reproduction;
- the evolution of virulence, etc.

There is lots of existing work on these general topics. Read papers from _Am Nat_, _Trends in Ecology and Evolution_, and _Ecology Letters_; journals that value generality, until you can describe the state-of-the art facts about a _general topic_, and explain their relevance for your study question. For example:

> Threshold growth quantities, such as R0, are equivalent in seasonal and non-seasonal environments when specific conditions, regarding reproduction and stage-to-stage transitions, are met (Mitchell and Kribs, 2017). While such conditions are met for sea lice, transient dynamics are key to understanding best management practices for sea lice, and so we aim to extend the current theory in this area.

* The first sentence describes a specific piece of knowledge about seasonal population dynamics. We show scholarship and build trust with our reviewers by demonstrating our knowledge of Mitchell and Kribs. The second sentence relates this general knowledge to our specific application.

6. It is good to read about the biology of your study species, but you need to spend at least as much time also reading about general ecological/evolutionary principles.

## Coding
1. If you are stuck on coding, perhaps the problem isn't your coding skills, but that don't know what you want to code. It may help to write out the steps of your code as pseudo-code.

1. Most graduate students in my lab are good coders. Talk it through with a labmate. Just saying it out loud can help you realize what you are trying to do isn't well defined.

1. Copy and paste the example from the help file. Then, step up the complexity of the problem, checking to see that it still runs after a small round of changes. Continue until you have modified the example code enough to solve your problem.

1. Find out the line your error is being generated from. Even in MATLAB, where the line of the error is reported, the actual error may have occurred before. You should be able to query the values assigned to quantities after each line is executed to identify the true line of the error.

1. I take a hypothesis testing, deductive reasoning, approach to debugging. For example, if a function has two arguments, then the error must be in the format/value of one or other argument, or with the function itself: run tests to narrow the possiblities.

1. Run unit tests: set the mean dispersal distance to zero; do you recover the expected non-spatial population dynamics? Set mortality to zero: does the population grow? Set the birth rate to zero: does the population die out?

1. Step through your code and query the value of your variables to make sure they are what you think they are.

1. I don't want you to be stuck on your code for long. I will help you, but it may take a week or so for me to find a gap in my schedule because this may require me to find a block time. I prefer to code-share with Github, Gitlab or similar version controlled softwares.

1. If you are stuck on your code, the problem may be that you don't have a good strategy for debugging your code. One way to learn this can be to sit with someone while they debug your code and have them explain what they are thinking as they perform different tests.

1. I expect you to publish your code alongside your manuscripts and thesis. Archive your code on Figshare, Github, Gitlab, or similar. There is a pipeline from Github to Zendodo and this may be the preferred way to publish your completed code.

1. The softwares I know(-ish) are MATLAB and R. MATLAB is still popular with mathematical biologists. R is very popular with biologists. R's user community is now so vast that there are packages available to do almost anything. R is free, which isn't a factor during your graduate studies, but may become a factor at your next place of employment. Although, I don't code in Python or C, you could complete your thesis in these languages if you are comfortable with them.

- The lab has a high performance computer called theo (see [here](https://amyhurford.weebly.com/theocsmunca.html) for instructions). If your simulations take a long time use theo or consider ACENET.

- There has been the suggestion to archive code and data from our lab all on one place (i.e. Github). I like this suggestion, but realistically, this is a low priority activity and I am unlikely to find time to do it in the near future. Please contact me if you'd be interested in helping out with this.

## Writing
1. The more familar you are with the published literature the better your writing will be, and the better you will be able to formulate hypotheses and explain your ideas.

1. Writing involves three steps: reading, thinking, and writing. After you read, you need to think about how this new knowledge ties in with your existing knowledge, and with the results you are writing about. If you don't know what to write, or you don't like what you've written, you may have skipped the thinking step.

1. Your paper has one main result. You did a lot of work to get to your results, but after doing all the work you need to organize it by prioritizing and de-priotizing certain elements.

1. The papers you cite need to fit your story. You probably don't want to write:

> Hurford et al. finds that wolves recolonize slower than expected.

A better sentence is:

> Efforts to parameterize and validate reaction-difussion and related spread rate models have generally found that empirical spread rates are slower than the theoretical predictions (Hurford et al.). 

This second sentence takes control of the narrative: to tell the story of my results, one important topic I need to discuss is 'parameterization and validation'. I will arrange my Introduction so that I discuss the _topics_ important to my results. The first example sentence typically appears in an Introduction that is unsure of the general topics that relate to the results (i.e., parameterization and validation). An Introduction written this way lists papers and their main results, and the focus is on citing the papers rather than building a narrative around your results.

1. Please write in active voice and present tense. For a Methods section it may be okay to write in past tense, but try to keep present tense as much as possible.

1. Please see [How to write a Nature Summary](https://cbs.umn.edu/sites/cbs.umn.edu/files/public/downloads/Annotated_Nature_abstract.pdf). Writing an abstract this way asks you to think about the broader relevance of your work, and this is often something that doesn't receive enough attention. 

1. Please see [Scientific paper outline by Kevin Lafferty](http://parasitology.msi.ucsb.edu/sites/parasitology.msi.ucsb.edu/files/docs/publications/Writing%20a%20Scientific%20Paper.doc). This outline asks the writer to provide paragraphs that show you have summarized the relevant literature on your topic. One component of this is the study system, but this comes after summarizing the literature on the 'big picture' question.

1. Start the outline of your paper by deciding what figures you will include. The writing needs to be concise, and planning this way will help keep a streamlined focus.

1. I recommend you learn Latex and write your thesis in it. Latex is stable, and MS Word usually crashes, especially when using equation editor in combination with copy and paste. 

1. Get feedback from me on your writing early and often. This helps you to understand my expectations for your written work. If you are hoping to quickly finish writing your thesis, the timeline is more likely to proceed as you expect if you are familiar with my expectations.

1. Get your peers to read your writing.

1. [How to write a theoretical paper that people will cite](http://www.eeb.cornell.edu/Ellner/) has good advice. 

1. Write concisely, and write the paper so that readers will find reading it interesting. Focus on explaining concepts and context; technical details are important, but they may belong in an Appendix. When proof-reading your work, consider whether the meaning of your sentence can be preserved, if some words are deleted. For example, 'the fact that, the earth is a sphere' can usually be edited down to 'the earth is a sphere': generally, 'the fact that' seems to add nothing.

1. Don't write things that aren't true or you aren't going to back up, for example, 'Climate change is the biggest problem of our generation'; if this is an ecology paper, you won't back up that sentence, that sentence belongs in an sociology paper.
