### Guiding principles

> Do meaningful work, that _you_ are proud of, and that _you_ are certain has value: editors and reviewers will often agree, but if they don't, seek solice in knowing that you didn't cut corners and you put in the work. Good work will eventually get rewarded, but the path to those rewards can sometimes be long. Don't judge yourself based on the achievements of your peers. Know _your_ strengths and appreciate them.

## Your thesis
### Getting started: Developing your hypotheses and models
1. Read the literature to formulate your hypotheses. Every thesis is framed within a broader context, for example,
- population dynamics in seasonal environments;
- population dynamics in stochastic environments;
- population dynamics given an earlier age of first reproduction;
- the evolution of virulence, etc.

There is lots of existing work on these general topics. Read papers from _Am Nat_, _TREE_, and _Ecology Letters_, journals that value generality, until you can describe the state-of-the art facts about a general topic, and explain their relevance for your study question. For example:

> Threshold growth quantities, such as R0, are equivalent in seasonal and non-seasonal environments when specific conditions, regarding reproduction and stage-to-stage transitions, are met (Mitchell and Kribs, 2017). While such conditions are met for sea lice population biology, transient dynamics are key to understanding best management practices for sea lice, and so we aim to extend the current theory in this area.

* The first sentence describes a specific piece of knowledge about seasonal population dynamics. We show scholarship and build trust with our reviewers by reading the paper, and summarizing the content relevant to our study. The second sentence relates this general knowledge to our specific application.

2. It is good to read about the biology of your study species, but you need to spend at least as much time also reading about general ecological/evolutionary principles.

3. Design a chapter that is do-able. In my experience, a plan that is 'just a bit more that the current state-of-the art', will turn into something bigger, and might even turn out to be unexpectedly amazing. A good thesis, is a done thesis. Make the hard decisions early: it is less glamorous to design a thesis that is feasible, but it will get you graduated and get you a job.

1. Getting a chapter finished is a great accomplishment.

1. Consider Zotero for manageing your references and the browser plug-in.

1. Here's advice on graduate school: https://dynamicecology.wordpress.com/2015/09/08/rereading-stearns-and-hueys-some-modest-advice-to-graduate-students/

1. Science is a team sport, but working with others takes effort and someone needs to organize and send the emails, which takes time. If you are comfortable with collaborating, it will benefit you later. Know the guidelines for authorship, and usually these relate to whoever is the lead giving the opportunity to others to be involved early enough. If you would like to involve a co-author, you need to discuss this with Amy.

### Now let's get some results: coding
1. While it can take time to read, think, and develop hypothesis, if you are stuck on coding you need to get help and get unstuck.
1. Perhaps the problem isn't your coding skills, but that don't know what you want to code. It may help to write out the steps of your code as pseudo-code.
1. Most graduate students in my lab are good coders. Talk it through with a labmate. Just saying it out loud can help you figure our your problem.
1. Copy and paste the example from the help file. Then, step up the complexity of the problem, checking to see that it still runs after a small round of changes.
1. find out the line your error is being generated from: even in MATLAB, where the line of the error is reported, the actual error may have occurred before.
1. I take a hypothesis testing, deductive reasoning approach to debugging. For example, if a function has two arguments, then the error must be in the format of one or other argument, or with the function itself: run tests to narrow the possiblities.
1. Run unit tests: set the mean dispersal distance to zero: do you recover the expected non-spatial population dynamics? Set mortality to zero: does the population grow? Set the birth rate to zero: does the population die out?
1. Step through your code and query the value of your variables, to make sure they are what you think they are.
1. I don't want you to be stuck on your code for long. I will help you, but it may take a week or so for me to find a gap in my schedule. I prefer to code-share with Github or similar version controlled softwares.
1. I expect you to publish your code alongside your manuscripts and thesis. Archive your code on Figshare or Github.
1. The softwares I know(-ish) are MATLAB and R. MATLAB was my first coding language, and it is still popular with mathematical biologists. But, I am beginning to prefer R, although my own coding in R is 'a bit of a mess'. MATLAB's strength is it's simplicity and the efficiency of some of the built-in algorithms, however, R's user community is now so vast, that I'm not sure MATLAB's algorithms are actually more efficient. I find R hard, because while MATLAB mostly uses matrices as data structures, R stores data in a variety of ways: dataframes, factors, numeric, etc. In addition, R is free.
- The lab has a high performance computer called theo. If you simulations take a long time, asking about using this, or consider ACENET.

### Let's disseminate those results: writing
1. The more familar you are with the published literature the better your writing will be, and the better you will be able to formulate hypotheses and explain your ideas.
1. Please write in active voice and present tense. For a Methods section is may be okay to write in past tense, but try to keep present tense as much as possible.
1. Please see 'How to write a Nature Summary'. Writing an abstract this way asks you to think about the broader relevance of your work and this is often something that doesn't receive enough attention. https://cbs.umn.edu/sites/cbs.umn.edu/files/public/downloads/Annotated_Nature_abstract.pdf
1. Please see 'Scientific paper outline by Kevin Lafferty' <http://parasitology.msi.ucsb.edu/sites/parasitology.msi.ucsb.edu/files/docs/publications/Writing%20a%20Scientific%20Paper.doc> This outline asks the writer to provide paragraphs that show you have summarized the relevant literature on your topic. One component of this is the study system, but this comes after summarizing the literature on the 'big picture' question.
1. Start the outline of your paper by deciding what figures you will include. The writing needs to be concise, and planning this way will help keep a streamlined focus.
1. I recommend you learn Latex and write your thesis in it. Latex is stable, and MS Word usually crashes.
1. Get feedback from me on your writing early. 
1. Get your peers to read your writing.
1. How to write a theoretical paper that people will cite: http://www.eeb.cornell.edu/Ellner/
1. Write concisely, and write the paper so that readers will find reading it interesting: respect the time of your readers, or risk losing their interest!
1. Don't write things that aren't true or you aren't going to back up just to fill space, for example, 'Climate change is biggest problem of our generation' - if this is an ecology paper, you won't back up that sentence: that sentence belongs in an sociology paper.
1. You need to think before you write. If you don't know what to write, or you don't like what you've written, you may have skipped the thinking step. After you get your results, think: why does this matter? Once you have an answer, then, you can write. If it's a review paper, after you've read all the papers on a topic think: how do these papers fit together?

### Let's disseminate those results: going to a conference
1. Your goal is to do good science. While graphic design and scientific communication are areas that you can read a lot on, unless your interests are specifically in these areas, remember to put the science first.
1. Having said that, this is quite good: http://basiliskos.com/downloads
1. The problem with explaining your work clearly is that the audience will understand what you've done and be able to ask you challenging questions. Don't fear the questions: you have to explain yourself clearly, constructive comments will help in the long-run.
1. Organize a lab meeting to practice your talk.
1. When answering questions, allow yourself to pause, and think of what your answer will be, before talking.
1. You may wish to use the lab logo.
1. Book your flights at least 6 weeks in advance. Select reasonably priced accommodation. Be aware of taxes and the currency of the price.
1. Submit a travel request form and having it signed by Amy: https://www.mun.ca/finance/forms/Travel_Request.pdf
1. If you would like reimbursement before you travel, submit a travel advance: https://www.mun.ca/finance/forms/Travel_Advance.pdf
1. Apply for travel funding from SGS (https://www.mun.ca/sgs/current/funding/travel.php), the biology department, TAUMAN, and the SMB Landahl fund.
1. Keep your receipts, except for food which is reimbursed at $50/day. Submit your travel claim: https://www.mun.ca/finance/travel/electronic_travel.php

## Your professional development
### Reviewing
The BES guide to reviewing is excellent.

### Working in industry
- The following is advice for a former student. This student found this blog plost helpful:
https://dynamicecology.wordpress.com/2014/10/27/non-academic-careers-for-ecologists-data-science-guest-post/
and adds 'most office jobs are posted at indeed.com and/or LinkedIn. Search by skills in addition to job titles. Different companies will have different names for their analyst roles, but if they are looking for someone who knows R/Python/MATLAB then it is worth a closer look.'
- You may wish to consider opporutnities offered through MITACS.


### Links
https://sites.google.com/a/d.umn.edu/professional-skills/
https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006914
https://twitter.com/thehauer/status/1021179403680862218

## Being a graduate student in the Biology Department

## Being a gradute student at MUN
https://www.mun.ca/edge/Prepare/

### Diversity, equity, and inclusion

### Funding
Even before you start at MUN you will need to request the TAships you want.

### Student visas
Make sure you apply for and keep your student visas current. Please contact the International Studies Office if you need help.

## Work-life balance
Traditionally, there has been a celebration of over-work in graduate studies, and the concept of work-life balance and self care have recently become known to be important. I leave it up to you when you work and I encourage you to seek balance and commununity during your studies.

The purpose of the committee meetings is to outline the volume of work and the timeframes for completion. For Masters and PhD degrees, funding is guaranteed for 2 and 4 years respectively. In addition, please note that you will need to submit your completed thesis several months in advance. Please click on the links under '2. Preparing for submission' for the  https://www.mun.ca/sgs/go/guid_policies/theses.php. All students are required to take BIOL 7000 and MSc students in biology are required to take two courses. One class should take 12 hrs/wk. In addition, two TAships is 120 hrs per semester (~10 hrs/wk). My hope is that providing you with these numbers will help  you plan your timeline to graduation.

### Mental health
Several articles draw attention to a mental health crisis amongst graduate students, for example, https://blogs.scientificamerican.com/observations/the-emotional-toll-of-graduate-school/. This article states _'the issues surrounding graduate student mental health are much easier to describe than to solve'_. Amongst the recommendations from this article:

- _'universities could require multiple advisors within a student’s field to evaluate degree timelines'_. This is the role of the supervisory committee for graduate students at MUN.

- _'Departments could also streamline their graduation criteria to reduce disparities in student workload amongst different research groups and to increase transparency of degree requirements'_. If you have questions regarding how much work is sufficient for a thesis, these should be brought up with the supervisory committee.

The supervisor-graduate student relationship can be a source of mental health challenges. As such, I have described my supervisory style below.

- If you are experiencing a mental health issue:
- Please let me know.
- At MUN, you may seek counselling by emailing gradcounselling@mun.ca (https://twitter.com/sgsdean/status/1176493016409923585/photo/1).
- You may wish to take a break from your studies.

### My supervisory style
I see my primary priority as to help you graduate your degree. My secondary priority is for the research contained in your thesis to be of as high quality as possible, to maximize your chances of success on the job market. In addition, the topic and research contained in the thesis needs to be within the scope of my research group, as discussed during our initial meetings.

The principle way that I supervise is by providing feedback on items you have completed. This may be a research proposal, or a written summary of your work during the past month, or a draft of a paper. Initially, I would like to have contact with each new student once every two weeks, but as our working relationship establishes, we can meet when there are completed items to discuss.

With regards to our working relationship you might consider the following:

- It can be difficult to tell your supervisor, or other graduate students that you don't know how to do something. Will they think you're are not smart and don't belong in graduate school? Isn't mathematical biology supposed to be what _your_ good at? You can make a lot of progress on your thesis by asking other graduate students for help with your coding, talking through the interpretation of a result, the logic behind a hypothesis, or getting feedback on your writing. The most sucessful graduate students are comfortable getting help from other gradaute students.

- Developing as an independent researcher is a goal of your studies. There is some element of being stuck that is a normal part of research, and overcoming the problem yourself may be the best approach for your professional development.

- You might want to schedule a meeting with me. If things are not working out like I said they would, then we need to figure out what is wrong. If you've just been stuck on that coding problem for too long, let me know to look at your code.

- The advice I have given you could be incorrect, and if you suspect that this is the case, we need to have a meeting to discuss the new best approach.

- Try to objectively consider which course of action is best for the situation you are in now. Aspects of the above advice are opposite: 'try to solve the problem yourself' and 'get help'.

There are some aspects of graduate student supervision that are unseen. I write grants to secure funding that goes toward your graduate student salary. I write reports on how these funds were used. I send emails to organize supervisory committees. I serve on the supervisory and examination committees of other graduate students. It isn't helpful to reflect on the one-on-one time we have spent to decide if I care about your success. I want you to succeed. I will write an honest letter of recommendation. 

## Mentoring plan
When you enter and after each year subsequently we will meet to discuss your career plans.
